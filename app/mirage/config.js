export default function() {
  this.namespace = 'api';
  this.get('/users/:id');
  this.get('/users');
  this.get('/chirps');
  this.get('/chirps/:id');
  this.post('/chirps');
}
